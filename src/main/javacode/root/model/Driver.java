package root.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.google.gson.annotations.Expose;




@Entity
public class Driver implements Serializable {
	
	
	private static final long serialVersionUID = -8434815471420170722L;
	
	@Id
	@Expose
	private String id_document;
	@Expose(serialize = false)
	private String password;
	@Expose
	private String full_name;
	@Expose
	private String phone_number;
	@Expose
	private String birthdate;
	@Expose
	private boolean has_disability;
	
	
	public String getBirthdate() {
		return birthdate;
	}
	public void setBirthdate(String birthdate) {
		this.birthdate = birthdate;
	}
	public boolean isHas_disability() {
		return has_disability;
	}
	public void setHas_disability(boolean has_disability) {
		this.has_disability = has_disability;
	}
	public String getId_document() {
		return id_document;
	}
	public void setId_document(String id_document) {
		this.id_document = id_document;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFull_name() {
		return full_name;
	}
	public void setFull_name(String full_name) {
		this.full_name = full_name;
	}
	public String getPhone_number() {
		return phone_number;
	}
	public void setPhone_number(String phone_number) {
		this.phone_number = phone_number;
	}
}
