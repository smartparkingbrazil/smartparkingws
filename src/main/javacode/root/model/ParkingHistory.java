package root.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.google.gson.annotations.Expose;

@Entity
public class ParkingHistory {
	
	@Id 
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int id;
	
	@OneToOne
	@Expose
	private Driver driver;
	@OneToOne
	@Expose
	private Spot spot;
	@Expose
	private Date dateEntry;
	@Expose
	private Date dateExit;
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public Spot getSpot() {
		return spot;
	}
	public void setSpot(Spot spot) {
		this.spot = spot;
	}
	public Date getDateEntry() {
		return dateEntry;
	}
	public void setDateEntry(Date dateEntry) {
		this.dateEntry = dateEntry;
	}
	public Date getDateExit() {
		return dateExit;
	}
	public void setDateExit(Date dateExit) {
		this.dateExit = dateExit;
	}
	
	
	
	
}
