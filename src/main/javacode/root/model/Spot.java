package root.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;

import com.google.gson.annotations.Expose;

@Entity
public class Spot implements Serializable{

	private static final long serialVersionUID = -1201328542901008561L;
	
	@Id 
	@Expose
	private String identifield; 
	@Expose
	private String type;
	@Expose
	private float latitude;
	@Expose
	private float longitude;
	@Expose
	private boolean driverIdentified;
	@Expose
	private boolean isOccupied;
	@Expose(serialize = false)
	@OneToOne
	private Driver driver;
	
	
	public Driver getDriver() {
		return driver;
	}
	public void setDriver(Driver driver) {
		this.driver = driver;
	}
	public String getIdentifield() {
		return identifield;
	}
	public void setIdentifield(String identifield) {
		this.identifield = identifield;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public float getLatitude() {
		return latitude;
	}
	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}
	public float getLongitude() {
		return longitude;
	}
	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}
	public boolean isDriverIdentified() {
		return driverIdentified;
	}
	public void setDriverIdentified(boolean driverIdentified) {
		this.driverIdentified = driverIdentified;
	}
	public boolean isOccupied() {
		return isOccupied;
	}
	public void setOccupied(boolean isOccupied) {
		this.isOccupied = isOccupied;
	}
	
}
