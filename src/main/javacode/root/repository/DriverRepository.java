package root.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import root.model.Driver;


public interface DriverRepository extends JpaRepository<Driver, String>{
	
	@Query("select u from Driver u where u.id_document = ?1")
	public Driver findById_document(String id_document);
	
	@Query("select u.id_document, u.full_name from Driver u")
	public Driver findAllNoPassword();
}
