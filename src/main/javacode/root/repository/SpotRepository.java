package root.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import root.model.Spot;

public interface SpotRepository extends JpaRepository<Spot, String> {

	public Spot findByIdentifield(String identifield);
	public Spot findByLatitudeAndLongitude(float latitude, float longitude);
}
