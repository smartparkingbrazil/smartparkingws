package root.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import root.model.ParkingHistory;

public interface ParkingHistoryRepository extends JpaRepository<ParkingHistory, Integer> {

}
