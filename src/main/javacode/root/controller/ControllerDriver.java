package root.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import root.model.Driver;
import root.service.DriverService;
import root.utils.SHAEncoder;

@RestController
public class ControllerDriver {
	
	public static final int DRIVER_INSERT_SUCCESS = 0;
    public static final int DRIVER_INSERT_ERROR = 1;
    public static final int DRIVER_INSERT_DUPLICATE = 2;
	
    public static final int DRIVER_RETRIEVE_SUCCESS = 0;
    public static final int DRIVER_NOT_FOUND = 1;
    public static final int DRIVER_WRONG_PASS = 3;
   
    public static final String ERROR_CODE_KEY = "error_code";
    
    @Autowired
	private DriverService driverService;
    private Gson gson = new Gson();
    private Gson gsonWithExpose = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
    
    
    @RequestMapping(
			value = "/clearDrivers",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String clearDriver(){
		return getClass().getSimpleName();
	}
	
	@RequestMapping(
			value = "/getDrivers",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String getDriver(){
		return getClass().getSimpleName();
	}
	
		
	@RequestMapping(
			value = "/login",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> loginDriver(@RequestBody String request){
		
		JsonObject json = gson.fromJson(request, JsonObject.class);
		JsonObject response = new JsonObject();		
		Driver driver = driverService.findById_document(json.get("id_document").getAsString());
		
		if(driver != null){
			
			String passwordEncode = new SHAEncoder().generateSHA(json.get("password").getAsString());
			
			if(driver.getPassword().equals(passwordEncode)){		
				
				response.add("driver", gsonWithExpose.toJsonTree(driver));
				response.addProperty("message", "Driver DB reading successfu");
				response.addProperty(ERROR_CODE_KEY, DRIVER_RETRIEVE_SUCCESS);
				
				return new ResponseEntity<String>(response.toString(), HttpStatus.OK); 
			}else {
				
				response.addProperty(ERROR_CODE_KEY, DRIVER_WRONG_PASS);
				response.addProperty("drvier", "");
				response.addProperty("message", "Wrong password");
				
				return new ResponseEntity<String>(response.toString(), HttpStatus.UNPROCESSABLE_ENTITY); 
			}			
		}else {
			
			response.addProperty(ERROR_CODE_KEY, DRIVER_NOT_FOUND);
			response.addProperty("drvier", "");
			response.addProperty("message", "Driver not found");
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.NOT_FOUND); 
		}
		
	}
	@RequestMapping(
			value = "/createDriver",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> createDriver(@RequestBody String request){
		
		JsonObject response = new JsonObject();
		Driver driver = gson.fromJson(request, Driver.class);
		
		if(driverService.existsDriver(driver)){
			
			response.addProperty(ERROR_CODE_KEY,DRIVER_INSERT_DUPLICATE);
			response.addProperty("message", "This ID is already in use:" + driver.getId_document());
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.CONFLICT); 
		}else{
			
			String pass = driver.getPassword();
			driver.setPassword(new SHAEncoder().generateSHA(pass));
			
			driverService.insert(driver);
			
			response.addProperty(ERROR_CODE_KEY,DRIVER_INSERT_SUCCESS);
			response.addProperty("message", "New driver registered:" + driver.getFull_name());
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.CREATED); 			
		}
	}
	
}
