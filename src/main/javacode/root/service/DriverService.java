package root.service;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import root.model.Driver;
import root.repository.DriverRepository;

@Service
public class DriverService {
	
	@Autowired
	private DriverRepository driverDAO;
	
	public void insert(Driver driver){
		driverDAO.save(driver);
	}
	public Driver findById_document(String id_document){
		return driverDAO.findById_document(id_document);
	
	}
	public boolean existsDriver(Driver driver){
		if(driverDAO.findById_document(driver.getId_document()) != null){
			return true;
		}else{
			return false;
		}
	}
}
