package root.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import root.model.ParkingHistory;
import root.repository.ParkingHistoryRepository;

@Service
public class ParkingHistoryService {
	@Autowired
	private ParkingHistoryRepository historyDAO;
	
	public void insert(ParkingHistory ph){
		historyDAO.save(ph);
	}
	public List<ParkingHistory> getHistory(){
		return historyDAO.findAll();
	}
}
