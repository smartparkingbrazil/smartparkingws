package root.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

import root.model.Driver;
import root.model.ParkingHistory;
import root.model.Spot;
import root.service.DriverService;
import root.service.ParkingHistoryService;
import root.service.SpotService;

@RestController
public class ControllerHistory {
	
	@Autowired
	private DriverService driverService;
	    
    @Autowired
    private ParkingHistoryService parkingService;
    
    @Autowired
    private SpotService spotService;
    
    private Gson gson = new Gson();
	private Gson gsonWithExpose = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
	public static final int PARKING_DONE = 0;
	public static final int PARKING_ERROR = 1;
	public static final String ERROR_CODE_KEY = "error_code";

	
	@RequestMapping(
			value = "/park",
			method = RequestMethod.POST,
			produces = MediaType.APPLICATION_JSON_VALUE,
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> parking(@RequestBody String request){
		
		JsonObject json = gson.fromJson(request, JsonObject.class);
		ParkingHistory ph = new ParkingHistory();
		JsonObject response = new JsonObject();	
		
		Driver driver = driverService.findById_document(json.get("driver_id").getAsString());
		Spot spot = spotService.findByID(json.get("spot_id").getAsString());
		
		if(driver == null || spot == null){
			
			ph.setSpot(spot);
			ph.setDateEntry(new Date());
			
			response.add("History", gsonWithExpose.toJsonTree(ph));
			response.addProperty("Message", "This spot is occupied by someone else!");
			response.addProperty(ERROR_CODE_KEY, PARKING_ERROR);
			return new ResponseEntity<String>(response.toString(),HttpStatus.OK);
		}else if(spot.isOccupied()){
			ph.setSpot(spot);
			ph.setDateEntry(new Date());
			
			response.add("History", gsonWithExpose.toJsonTree(ph));
			response.addProperty("Message", "This spot or This driver not exist!");
			response.addProperty(ERROR_CODE_KEY, PARKING_ERROR);
			return new ResponseEntity<String>(response.toString(),HttpStatus.OK);
		}else if(spotService.isEspcialSpot(spot) && !driverService.isEspecialDriver(driver)){
			ph.setSpot(spot);
			ph.setDateEntry(new Date());
			
			response.add("History", gsonWithExpose.toJsonTree(ph));
			response.addProperty("Message", "This spot is especial and This driver not have permission!");
			response.addProperty(ERROR_CODE_KEY, PARKING_ERROR);
			return new ResponseEntity<String>(response.toString(),HttpStatus.OK);
		}else{
			spot.setDriver(driver);
			//spot.setOccupied(true);
			spot.setDriverIdentified(true);
			
			ph.setDriver(driver);
			ph.setSpot(spot);
			ph.setDateEntry(new Date());
			
			spotService.insert(spot);
			parkingService.insert(ph);
			
			response.add("History", gsonWithExpose.toJsonTree(ph));
			response.addProperty(ERROR_CODE_KEY, PARKING_DONE);
			response.addProperty("Message", "Historico successfully altered!");
			return new ResponseEntity<String>(response.toString(),HttpStatus.CREATED);
		}
							
	}
	
	@RequestMapping(
			value = "/getCurrentState",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSpot(){
		
		List<Spot> spots = spotService.findALL();
		JsonObject response = new JsonObject();	
		
		if(spots.isEmpty()){
			
			response.addProperty("Message", "There is no Spot created yet!");
			response.addProperty(ERROR_CODE_KEY, PARKING_ERROR);
			response.add("Spots",gson.toJsonTree(spots));
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
		}else{
			
			response.addProperty("Message", "Spots of your parking!");
			response.addProperty(ERROR_CODE_KEY, PARKING_DONE);
			response.add("Spots", gson.toJsonTree(spots));
			
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
		}
	}
	
	@RequestMapping(
			value = "/getHistory",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getHistory(){
		
		JsonObject response = new JsonObject();	
		List<ParkingHistory> phs = parkingService.getHistory();
		if (phs.isEmpty()) {
			
			response.addProperty("Message", "There is no parking history");
			response.addProperty(ERROR_CODE_KEY, PARKING_ERROR);
			response.add("History",gsonWithExpose.toJsonTree(phs));
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
		}else{
			
			response.addProperty("Message", "History of your parking!");
			response.addProperty(ERROR_CODE_KEY, PARKING_DONE);
			response.add("History", gsonWithExpose.toJsonTree(phs));
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
		}
		
	}
}
