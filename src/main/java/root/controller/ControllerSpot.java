package root.controller;



import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;


import root.model.Spot;
import root.service.SpotService;
import root.utils.FiwareHandler;

@RestController
public class ControllerSpot {
	
	@Autowired
	private SpotService spotService;
	
	private FiwareHandler fiware = new FiwareHandler();
	
	private Gson gson = new Gson();
	private Gson gsonWithExpose = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
	
	public static final String ERROR_CODE_KEY = "error_code";
	public static final int SPOT_DENIED = 1;
	public static final int SPOT_SUCCESS = 0;
	
	@RequestMapping(
			value = "/updatedbSpots",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String updatedbSpot(){
		return getClass().getSimpleName();
	}
	
	@RequestMapping(
			value = "/clearSpots",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public String clearSpot(){
		return getClass().getSimpleName();
	}
	
	@RequestMapping(
			value = "/getSpots",
			method = RequestMethod.GET,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> getSpot(){
		
		List<Spot> spots = spotService.findALL();
		JsonObject response = new JsonObject();	
		
		if(spots.isEmpty()){
			
			response.addProperty("Message", "There is no Spot created yet!");
			response.addProperty(ERROR_CODE_KEY, SPOT_DENIED);
			response.add("Spots",gsonWithExpose.toJsonTree(spots));
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.NOT_FOUND);
		}else{
			
			response.addProperty("Message", "Spots of your parking!");
			response.addProperty(ERROR_CODE_KEY, SPOT_SUCCESS);
			response.add("Spots", gsonWithExpose.toJsonTree(spots));
			
			return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
		}
	}
	
	
	
	@RequestMapping(
			value = "/createSpot",
			method = RequestMethod.POST,
			consumes = MediaType.APPLICATION_JSON_VALUE,
			produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> CreateSpot(@RequestBody String request){
		
		Spot spot = gson.fromJson(request, Spot.class);
		JsonObject response = new JsonObject();	
		
		if(spotService.exitsSpot(spot)){
			
			response.addProperty("message", "Conflicting Parameters! Repeated ID or something with your own coodenadas!");
			response.addProperty(ERROR_CODE_KEY, SPOT_DENIED);
			response.add("Spot", gson.toJsonTree(spot));
			return new ResponseEntity<String>(response.toString(), HttpStatus.CONFLICT); 
			
		}else{
			
			
			spotService.insert(spot);
			String fiwareResponse = fiware.createSpot(spot);
			
			response.addProperty(ERROR_CODE_KEY, SPOT_SUCCESS);
			response.addProperty("message", "Spot created successfully!");
			response.add("Spot", gson.toJsonTree(spot));
			response.addProperty("SpotFiware",fiwareResponse);
			return new ResponseEntity<String>(response.toString(), HttpStatus.CREATED);
		}		
		
	}
}
