package root.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import root.model.Spot;
import root.repository.SpotRepository;

@Service
public class SpotService {
	
	@Autowired
	private SpotRepository spotDAO;
	
	public Spot findByID(String id){
		return spotDAO.findByIdentifield(id);
	}
	public void insert(Spot spot){
		spotDAO.save(spot);
	}
	public List<Spot> findALL(){
		return spotDAO.findAll();
	}
	public boolean exitsSpot(Spot spot){
		if(spotDAO.findByIdentifield(spot.getIdentifield()) != null 
				|| spotDAO.findByLatitudeAndLongitude(spot.getLatitude(), spot.getLongitude()) != null){
			return true;
		}else{
			return false;
		}
	}
	public boolean isEspcialSpot(Spot spot){
		if(spot.getType().matches("handicapped|elderly")){
			return true;
		}else{
			return false;
		}
	}
}
