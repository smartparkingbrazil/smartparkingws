package root.service;




import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import root.model.Driver;
import root.repository.DriverRepository;

@Service
public class DriverService {
	
	@Autowired
	private DriverRepository driverDAO;
	
	public void insert(Driver driver){
		driverDAO.save(driver);
	}
	public Driver findById_document(String id_document){
		return driverDAO.findById_document(id_document);
	
	}
	public boolean existsDriver(Driver driver){
		if(driverDAO.findById_document(driver.getId_document()) != null){
			return true;
		}else{
			return false;
		}
	}
	public boolean isEspecialDriver(Driver driver){
		if(driver.isHas_disability() || ageDriver(driver.getBirthdate()) > 60){
			return true;
		}else{
			return false;
		}
	}
	private long ageDriver(String birthdate){
		SimpleDateFormat df = new SimpleDateFormat ("dd-MM-yyyy");
		long age = 0;
		try {
			Date date =  df.parse(birthdate);
			Date dateNow = new Date();
			long day = (dateNow.getTime() - date.getTime()) / 86400000L;
			age = day / 365;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return age;			
	}
}
