package root.utils;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import root.model.Spot;

public class FiwareHandler {
	
	private RestTemplate rest = new RestTemplate();
	private String url = "http://45.55.40.43:1026/v1/updateContext";
	
	public String createSpot(Spot spot){
		JsonObject request = new JsonObject();
		
		JsonArray contextElement = new JsonArray();
		
		JsonObject jsonElements = new JsonObject();
		jsonElements.addProperty("type", "Spot");
		jsonElements.addProperty("id", spot.getIdentifield());
		jsonElements.addProperty("isPattern","false");
		
		JsonArray attributes = new JsonArray();
		
		JsonObject param1 = new JsonObject();
		param1.addProperty("name", "driverIdentified");
		param1.addProperty("type", "boolean");
		param1.addProperty("value", false);
		
		JsonObject param2 = new JsonObject();
		param2.addProperty("name", "isOccupied");
		param2.addProperty("type", "boolean");
		param2.addProperty("value", false);
		
		JsonObject param3 = new JsonObject();
		param3.addProperty("name", "latitude");
		param3.addProperty("type", "float");
		param3.addProperty("value", Float.toString(spot.getLatitude()));
		
		JsonObject param4 = new JsonObject();
		param4.addProperty("name", "longitude");
		param4.addProperty("type", "float");
		param4.addProperty("value", Float.toString(spot.getLongitude()));
		
		JsonObject param5 = new JsonObject();
		param5.addProperty("name", "type");
		param5.addProperty("type", "string");
		param5.addProperty("value", spot.getType());
		
		
		attributes.add(param1);		
		attributes.add(param2);
		attributes.add(param3);
		attributes.add(param4);
		attributes.add(param5);
		
		jsonElements.add("attributes", attributes);
		contextElement.add(jsonElements);	
		
		
		request.add("contextElements", contextElement);
		request.addProperty("updateAction", "APPEND");
		
		HttpHeaders header = new HttpHeaders();
		header.setContentType(MediaType.APPLICATION_JSON);
		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), header);
		rest = new RestTemplate();
		return rest.postForObject(url, entity, String.class );
		
	}
}
